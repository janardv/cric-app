<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FieldStatistic
 *
 * @ORM\Table(name="field_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FieldStatisticRepository")
 */
class FieldStatistic
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="catch_count", type="integer")
     */
    private $catchCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="run_out_count", type="integer")
     */
    private $runOutCount = 0;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set catchCount
     *
     * @param integer $catchCount
     *
     * @return FieldStatistic
     */
    public function setCatchCount($catchCount)
    {
        $this->catchCount = $catchCount;

        return $this;
    }

    /**
     * Get catchCount
     *
     * @return int
     */
    public function getCatchCount()
    {
        return $this->catchCount;
    }

    /**
     * Set runOutCount
     *
     * @param integer $runOutCount
     *
     * @return FieldStatistic
     */
    public function setRunOutCount($runOutCount)
    {
        $this->runOutCount = $runOutCount;

        return $this;
    }

    /**
     * Get runOutCount
     *
     * @return int
     */
    public function getRunOutCount()
    {
        return $this->runOutCount;
    }
}

