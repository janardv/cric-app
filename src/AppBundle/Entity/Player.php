<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Player
 *
 * @ORM\Table(name="players")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlayerRepository")
 */
class Player
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="BatStatistic")
     * @ORM\JoinColumn(name="bat_statistic_id", referencedColumnName="id")
     */
    private $batStatistic;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="BowlStatistic")
     * @ORM\JoinColumn(name="bowl_statistic_id", referencedColumnName="id")
     */
    private $bowlStatistic;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="FieldStatistic")
     * @ORM\JoinColumn(name="field_statistic_id", referencedColumnName="id")
     */
    private $fieldStatistic;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="team_id", referencedColumnName="id")
     */
    private $team;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set batStatistic
     *
     * @param \AppBundle\Entity\BatStatistic $batStatistic
     *
     * @return Player
     */
    public function setBatStatistic(\AppBundle\Entity\BatStatistic $batStatistic = null)
    {
        $this->batStatistic = $batStatistic;

        return $this;
    }

    /**
     * Get batStatistic
     *
     * @return \AppBundle\Entity\BatStatistic
     */
    public function getBatStatistic()
    {
        return $this->batStatistic;
    }

    /**
     * Set bowlStatistic
     *
     * @param \AppBundle\Entity\BowlStatistic $bowlStatistic
     *
     * @return Player
     */
    public function setBowlStatistic(\AppBundle\Entity\BowlStatistic $bowlStatistic = null)
    {
        $this->bowlStatistic = $bowlStatistic;

        return $this;
    }

    /**
     * Get bowlStatistic
     *
     * @return \AppBundle\Entity\BowlStatistic
     */
    public function getBowlStatistic()
    {
        return $this->bowlStatistic;
    }

    /**
     * Set fieldStatistic
     *
     * @param \AppBundle\Entity\FieldStatistic $fieldStatistic
     *
     * @return Player
     */
    public function setFieldStatistic(\AppBundle\Entity\FieldStatistic $fieldStatistic = null)
    {
        $this->fieldStatistic = $fieldStatistic;

        return $this;
    }

    /**
     * Get fieldStatistic
     *
     * @return \AppBundle\Entity\FieldStatistic
     */
    public function getFieldStatistic()
    {
        return $this->fieldStatistic;
    }

    /**
     * Set team
     *
     * @param \AppBundle\Entity\Team $team
     *
     * @return Player
     */
    public function setTeam(\AppBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeam()
    {
        return $this->team;
    }
}
