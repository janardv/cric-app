<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Wicket
 *
 * @ORM\Table(name="wickets")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\WicketRepository")
 */
class Wicket
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="wicket_of", referencedColumnName="id")
     */
    private $wicketOf;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="taken_by", referencedColumnName="id")
     */
    private $takenBy;

    /**
     * @var int
     * 
     * @ORM\ManyToOne(targetEntity="WicketType")
     * @ORM\JoinColumn(name="wicket_type", referencedColumnName="id")
     */
    private $wicketType;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set wicketOf
     *
     * @param \AppBundle\Entity\Player $wicketOf
     *
     * @return Wicket
     */
    public function setWicketOf(\AppBundle\Entity\Player $wicketOf = null)
    {
        $this->wicketOf = $wicketOf;

        return $this;
    }

    /**
     * Get wicketOf
     *
     * @return \AppBundle\Entity\Player
     */
    public function getWicketOf()
    {
        return $this->wicketOf;
    }

    /**
     * Set takenBy
     *
     * @param \AppBundle\Entity\Player $takenBy
     *
     * @return Wicket
     */
    public function setTakenBy(\AppBundle\Entity\Player $takenBy = null)
    {
        $this->takenBy = $takenBy;

        return $this;
    }

    /**
     * Get takenBy
     *
     * @return \AppBundle\Entity\Player
     */
    public function getTakenBy()
    {
        return $this->takenBy;
    }

    /**
     * Set wicketType
     *
     * @param \AppBundle\Entity\WicketType $wicketType
     *
     * @return Wicket
     */
    public function setWicketType(\AppBundle\Entity\WicketType $wicketType = null)
    {
        $this->wicketType = $wicketType;

        return $this;
    }

    /**
     * Get wicketType
     *
     * @return \AppBundle\Entity\WicketType
     */
    public function getWicketType()
    {
        return $this->wicketType;
    }
}
