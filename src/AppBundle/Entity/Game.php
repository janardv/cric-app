<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 *
 * @ORM\Table(name="games")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 */
class Game
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="team_a", referencedColumnName="id")
     */
    private $teamA;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Team")
     * @ORM\JoinColumn(name="team_b", referencedColumnName="id")
     */
    private $teamB;

    /**
     * @var string
     *
     * @ORM\Column(name="result", type="string", length=20)
     */
    private $result;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set result
     *
     * @param string $result
     *
     * @return Game
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set teamA
     *
     * @param \AppBundle\Entity\Team $teamA
     *
     * @return Game
     */
    public function setTeamA(\AppBundle\Entity\Team $teamA = null)
    {
        $this->teamA = $teamA;

        return $this;
    }

    /**
     * Get teamA
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeamA()
    {
        return $this->teamA;
    }

    /**
     * Set teamB
     *
     * @param \AppBundle\Entity\Team $teamB
     *
     * @return Game
     */
    public function setTeamB(\AppBundle\Entity\Team $teamB = null)
    {
        $this->teamB = $teamB;

        return $this;
    }

    /**
     * Get teamB
     *
     * @return \AppBundle\Entity\Team
     */
    public function getTeamB()
    {
        return $this->teamB;
    }
}
