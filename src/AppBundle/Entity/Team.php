<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 *
 * @ORM\Table(name="teams")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="team_side", type="string", length=50)
     */
    private $teamSide;

    /**
     * @var int
     *
     * @ORM\Column(name="team_score", type="integer")
     */
    private $teamScore;

    /**
     * @var int
     *
     * @ORM\Column(name="team_wickets", type="integer")
     */
    private $teamWickets;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Player")
     * @ORM\JoinColumn(name="highest_scorer", referencedColumnName="id")
     */
    private $highestScorer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teamSide
     *
     * @param string $teamSide
     *
     * @return Team
     */
    public function setTeamSide($teamSide)
    {
        $this->teamSide = $teamSide;

        return $this;
    }

    /**
     * Get teamSide
     *
     * @return string
     */
    public function getTeamSide()
    {
        return $this->teamSide;
    }

    /**
     * Set teamScore
     *
     * @param integer $teamScore
     *
     * @return Team
     */
    public function setTeamScore($teamScore)
    {
        $this->teamScore = $teamScore;

        return $this;
    }

    /**
     * Get teamScore
     *
     * @return integer
     */
    public function getTeamScore()
    {
        return $this->teamScore;
    }

    /**
     * Set teamWickets
     *
     * @param integer $teamWickets
     *
     * @return Team
     */
    public function setTeamWickets($teamWickets)
    {
        $this->teamWickets = $teamWickets;

        return $this;
    }

    /**
     * Get teamWickets
     *
     * @return integer
     */
    public function getTeamWickets()
    {
        return $this->teamWickets;
    }

    /**
     * Set highestScorer
     *
     * @param \AppBundle\Entity\Player $highestScorer
     *
     * @return Team
     */
    public function setHighestScorer(\AppBundle\Entity\Player $highestScorer = null)
    {
        $this->highestScorer = $highestScorer;

        return $this;
    }

    /**
     * Get highestScorer
     *
     * @return \AppBundle\Entity\Player
     */
    public function getHighestScorer()
    {
        return $this->highestScorer;
    }
}
