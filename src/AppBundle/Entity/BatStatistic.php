<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BatStatistic
 *
 * @ORM\Table(name="bat_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BatStatisticRepository")
 */
class BatStatistic {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="balls_faced", type="integer")
     */
    private $ballsFaced = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="score", type="integer")
     */
    private $score = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="single_count", type="integer")
     */
    private $singleCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="double_count", type="integer")
     */
    private $doubleCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="triple_count", type="integer")
     */
    private $tripleCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="four_count", type="integer")
     */
    private $fourCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="six_count", type="integer")
     */
    private $sixCount = 0;

    /**
     * @var bool
     *
     * @ORM\Column(name="wicket_status", type="boolean")
     */
    private $wicketStatus = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set ballsFaced
     *
     * @param integer $ballsFaced
     *
     * @return BatStatistic
     */
    public function setBallsFaced($ballsFaced) {
        $this->ballsFaced = $ballsFaced;

        return $this;
    }

    /**
     * Get ballsFaced
     *
     * @return int
     */
    public function getBallsFaced() {
        return $this->ballsFaced;
    }

    /**
     * Set score
     *
     * @param integer $score
     *
     * @return BatStatistic
     */
    public function setScore($score) {
        $this->score = $score;

        return $this;
    }

    /**
     * Get score
     *
     * @return int
     */
    public function getScore() {
        return $this->score;
    }

    /**
     * Set singleCount
     *
     * @param integer $singleCount
     *
     * @return BatStatistic
     */
    public function setSingleCount($singleCount) {
        $this->singleCount = $singleCount;

        return $this;
    }

    /**
     * Get singleCount
     *
     * @return int
     */
    public function getSingleCount() {
        return $this->singleCount;
    }

    /**
     * Set doubleCount
     *
     * @param integer $doubleCount
     *
     * @return BatStatistic
     */
    public function setDoubleCount($doubleCount) {
        $this->doubleCount = $doubleCount;

        return $this;
    }

    /**
     * Get doubleCount
     *
     * @return int
     */
    public function getDoubleCount() {
        return $this->doubleCount;
    }

    /**
     * Set tripleCount
     *
     * @param integer $tripleCount
     *
     * @return BatStatistic
     */
    public function setTripleCount($tripleCount) {
        $this->tripleCount = $tripleCount;

        return $this;
    }

    /**
     * Get tripleCount
     *
     * @return int
     */
    public function getTripleCount() {
        return $this->tripleCount;
    }

    /**
     * Set fourCount
     *
     * @param integer $fourCount
     *
     * @return BatStatistic
     */
    public function setFourCount($fourCount) {
        $this->fourCount = $fourCount;

        return $this;
    }

    /**
     * Get fourCount
     *
     * @return int
     */
    public function getFourCount() {
        return $this->fourCount;
    }

    /**
     * Set sixCount
     *
     * @param integer $sixCount
     *
     * @return BatStatistic
     */
    public function setSixCount($sixCount) {
        $this->sixCount = $sixCount;

        return $this;
    }

    /**
     * Get sixCount
     *
     * @return int
     */
    public function getSixCount() {
        return $this->sixCount;
    }

    /**
     * Set wicketStatus
     *
     * @param boolean $wicketStatus
     *
     * @return BatStatistic
     */
    public function setWicketStatus($wicketStatus) {
        $this->wicketStatus = $wicketStatus;

        return $this;
    }

    /**
     * Get wicketStatus
     *
     * @return bool
     */
    public function getWicketStatus() {
        return $this->wicketStatus;
    }

}
