<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BowlStatistic
 *
 * @ORM\Table(name="bowl_statistics")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BowlStatisticRepository")
 */
class BowlStatistic {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="balls_bowled", type="integer")
     */
    private $ballsBowled = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wickets", type="integer")
     */
    private $wickets = 0;

    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Extras")
     * @ORM\JoinColumn(name="extras_id", referencedColumnName="id")
     */
    private $extras = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="runs_conceded", type="integer")
     */
    private $runsConceded = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="maiden", type="integer")
     */
    private $maiden = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set ballsBowled
     *
     * @param integer $ballsBowled
     *
     * @return BowlStatistic
     */
    public function setBallsBowled($ballsBowled) {
        $this->ballsBowled = $ballsBowled;

        return $this;
    }

    /**
     * Get ballsBowled
     *
     * @return integer
     */
    public function getBallsBowled() {
        return $this->ballsBowled;
    }

    /**
     * Set wickets
     *
     * @param integer $wickets
     *
     * @return BowlStatistic
     */
    public function setWickets($wickets) {
        $this->wickets = $wickets;

        return $this;
    }

    /**
     * Get wickets
     *
     * @return integer
     */
    public function getWickets() {
        return $this->wickets;
    }

    /**
     * Set runsConceded
     *
     * @param integer $runsConceded
     *
     * @return BowlStatistic
     */
    public function setRunsConceded($runsConceded) {
        $this->runsConceded = $runsConceded;

        return $this;
    }

    /**
     * Get runsConceded
     *
     * @return integer
     */
    public function getRunsConceded() {
        return $this->runsConceded;
    }

    /**
     * Set maiden
     *
     * @param integer $maiden
     *
     * @return BowlStatistic
     */
    public function setMaiden($maiden) {
        $this->maiden = $maiden;

        return $this;
    }

    /**
     * Get maiden
     *
     * @return integer
     */
    public function getMaiden() {
        return $this->maiden;
    }

    /**
     * Set extras
     *
     * @param \AppBundle\Entity\Extras $extras
     *
     * @return BowlStatistic
     */
    public function setExtras(\AppBundle\Entity\Extras $extras = null) {
        $this->extras = $extras;

        return $this;
    }

    /**
     * Get extras
     *
     * @return \AppBundle\Entity\Extras
     */
    public function getExtras() {
        return $this->extras;
    }

}
