<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Extras
 *
 * @ORM\Table(name="extras")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExtrasRepository")
 */
class Extras {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="total_extras", type="integer")
     */
    private $totalExtras = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="wide_count", type="integer")
     */
    private $wideCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="no_ball_count", type="integer")
     */
    private $noBallCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="byes_count", type="integer")
     */
    private $byesCount = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="leg_byes_count", type="integer")
     */
    private $legByesCount = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set totalExtras
     *
     * @param integer $totalExtras
     *
     * @return Extras
     */
    public function setTotalExtras($totalExtras) {
        $this->totalExtras = $totalExtras;

        return $this;
    }

    /**
     * Get totalExtras
     *
     * @return int
     */
    public function getTotalExtras() {
        return $this->totalExtras;
    }

    /**
     * Set wideCount
     *
     * @param integer $wideCount
     *
     * @return Extras
     */
    public function setWideCount($wideCount) {
        $this->wideCount = $wideCount;

        return $this;
    }

    /**
     * Get wideCount
     *
     * @return int
     */
    public function getWideCount() {
        return $this->wideCount;
    }

    /**
     * Set noBallCount
     *
     * @param integer $noBallCount
     *
     * @return Extras
     */
    public function setNoBallCount($noBallCount) {
        $this->noBallCount = $noBallCount;

        return $this;
    }

    /**
     * Get noBallCount
     *
     * @return int
     */
    public function getNoBallCount() {
        return $this->noBallCount;
    }

    /**
     * Set byesCount
     *
     * @param integer $byesCount
     *
     * @return Extras
     */
    public function setByesCount($byesCount) {
        $this->byesCount = $byesCount;

        return $this;
    }

    /**
     * Get byesCount
     *
     * @return int
     */
    public function getByesCount() {
        return $this->byesCount;
    }

    /**
     * Set legByesCount
     *
     * @param integer $legByesCount
     *
     * @return Extras
     */
    public function setLegByesCount($legByesCount) {
        $this->legByesCount = $legByesCount;

        return $this;
    }

    /**
     * Get legByesCount
     *
     * @return int
     */
    public function getLegByesCount() {
        return $this->legByesCount;
    }

}
